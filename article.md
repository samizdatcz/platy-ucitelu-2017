title: "Detailní data o platech, odměnách a věku učitelů: Systém motivuje padesátníky a odrazuje mladé"
perex: ""
authors: ["Jan Boček"]
published: "30. června 2017"
coverimg: https://www.irozhlas.cz/sites/default/files/styles/zpravy_snowfall/public/uploader/profimedia-032924401_170612-221538_per.jpg?itok=X0yEeW_j
coverimg_note: "Foto: Fotobanka Profimedia | <a href='https://www.irozhlas.cz/sites/default/files/styles/zpravy_snowfall/public/uploader/profimedia-032924401_170612-221538_per.jpg?itok=X0yEeW_j'>iRozhlas</a>"
styles: []
libraries: ["https://unpkg.com/jquery@3.2.1", "https://code.highcharts.com/highcharts.js"]
options: "" #wide
---

**Nedávno vydaná analýza think tanku IDEA prozradila, že čeští učitelé na základních a středních školách jsou na tom platově nejhůř z vyspělých zemí. Podrobnější data ukazují, jak české školství peníze rozděluje.**

Čeští učitelé na základních a středních školách si loni měli průměrný hrubý plat 29 882 korun. Kdyby se stejným vzděláním zakotvili jinde ve veřejné sféře, brali by o dvacet procent víc. V soukromém sektoru by si přilepšili o čtyřicet procent. Ukazují to čísla Ministerstva financí.

<wide><div id="strukturaplatu" style="width:100%; height:600px"></div></wide>

„V situaci, kdy je historicky nejnižší nezaměstnanost, platy v soukromé sféře rostou a potřeba získat nové lidi též,“ popisuje motivace učitelů expert na školství Bob Kartous ze společnosti Eduin. „Roste tak motivace učitelů odejít. To může ještě zhoršit nedostatek učitelek a učitelů. Už teď je situace téměř neudržitelná z toho důvodu, že ze zákona smí být učitelem jen lidi s pedagogickým vzděláním.“

Data o struktuře platu navíc podle Kartouse odhalují další slabinu. „Oproti jiným oblastem je u učitelů významně silnější fixní část platu, což znemožňuje odměňovat učitele podle reálného výkonu. Proto jsou učitelé odměňováni relativně rovnostářsky, ať už jsou dobří, či nikoliv. Ředitelé škol mají velmi svázané ruce. Nemají vlastně téměř žádné nástroje, pokud by chtěli dobré učitele odměnit lépe. Problém je také v tom, že doposud neexistuje profil žádoucích profesních kvalit učitelů a odměňování tak může být zcela nahodilé nebo sklouzávat k oceňování věrnosti a loajality, nikoliv pedagogické kvality,“ dodává Kartous.

Variabilní složka platu je u učitelů skutečně velmi nízká: odměny loni tvořily průměrně 6,3 procenta hrubých příjmů, ve veřejné sféře to bylo 9 procent a v soukromé 18 procent.

Vývoj platů také ukazuje, jak učitele základních a středních škol – předškolní a vysokoškolské v této statistice neukazujeme – zasáhla ekonomická krize. Přestože základní část platu, o kterou se vedou politické spory, vytrvale rostla, odměny a příplatky se ve stejném období snížily. Příjmy se proto mezi roky 2008 a 2012 učitelům zvedly jen o několik stovek korun. (Mezi zaměstnanci soukromé sféry s nižším vzděláním ovšem ve stejné době platy mírně klesly.)

*V grafech ukazujeme vždy průměr, přestože pro platy by se lépe hodil medián. Je to proto, že data o odměnách a příplatcích zveřejňuje Ministerstvo financí jako průměrná čísla. Zejména u soukromé sféry se medián platu, který lépe popisuje typického zaměstnance, pohybuje o několik tisíc korun pod průměrem.*

## Česko beznadějně poslední

V polovině letošního června vydal Institut pro demokracii a ekonomickou analýzu [studii](https://idea.cerge-ei.cz/zpravy/platy-ucitelu-ceskych-zakladnich-skol-setrvale-nizke-a-neatraktivni), ve které upozorňuje, že čeští učitelé v primárním a sekundárním školství jsou nejhůř placení ze všech vyspělých zemí sdružených v OECD. Podle dat této organizace mají příjmy na úrovni 56 procent oproti všem vysokoškolsky vzdělaným, průměr OECD je na 86 procentech.

<wide><div id="svet" style="width:100%; height:600px"></div></wide>

Stejná data mimochodem ukazují, že platově ještě hůř jsou na tom učitelé v mateřských školách. Pro lepší přehlednost je graf ukazuje až po kliknutí na *MŠ* v legendě ve spodní části grafu. Stejným způsobem lze některou z položek skrýt.

„Průměrné platy učitelů se v současnosti pohybují na 105 až 110 procentech průměrné mzdy, tedy na obdobné úrovni jako v roce 1995,“ upozorňuje autor studie Daniel Münich. „To je velmi málo s ohledem na to, že většina českých zaměstnanců nemá vysokoškolské vzdělání, které naopak učitelé dnes musejí mít. Alternativní ukazatele učitelských platů na neutěšené mezinárodní pozici Česka nic nemění. Důsledkem je velký nedostatek učitelů, těch mladších především, a pomalu a jistě klesající kvalita učitelského sboru.“

## První stupeň: jeden muž na třicet žen

Data Ministerstva financí umožňují podívat se na strukturu učitelských platů například podle věku, pohlaví nebo doby odsloužené na jedné škole. Nejprve u učitelů 1. stupně základních škol.

Kvůli přehlednosti mají grafy opět skrytou položku; na podíl, respektive počet učitelů se můžete podívat kliknutím do legendy grafu.

<wide><div id="1stupenvek" style="width:100%; height:600px"></div></wide>

<wide><div id="1stupenpohlavi" style="width:100%; height:600px"></div></wide>

<wide><div id="1stupenzamestnani" style="width:100%; height:600px"></div></wide>

Podrobná data o platech ukazují, že

* Platy učitelů rostou s věkem prakticky lineárně. Rozdíl způsobuje hlavně nárůst fixní složky platu. Jinak se chovají příplatky za nejrůznější činnosti, jako je třídnictví – ty rostou až zhruba do padesáti let, pak začínají klesat. Naopak odměny jsou celý život v podstatě konstantní; to potvrzuje, že se ředitelé snaží učitelům dávat maximální odměny bez ohledu na další okolnosti.
* Typickému učiteli je mezi padesátkou a šedesátkou. V tomto věku učí na základní škole víc než jedno procento každého populačního ročníku. Naopak mladých učitelů je málo, navíc po 27. roce začnou ze škol mizet. Příčina je banální: vzhledem k vysokému podílu žen jde zejména o odchody na mateřskou. Do školství se ovšem vrací jen malá část z nich a učitelů mezi třicítkou a čtyřicítkou je proto velmi málo.
* K podílu žen: dosahuje neuvěřitelných 97 procent, na jednoho učitele je na prvním stupni víc než třicet učitelek.
* Průměrný plat mužů je zhruba o tisícovku vyšší než u žen. Důvodem je fakt, že muži na prvním stupni se častěji ocitají na pozici ředitele nebo jeho zástupce.

## Druhý stupeň: vyšší plat a více třicátníků

Stejná data jsou k dispozici také pro učitele na druhém stupni základních škol a čtyř- i víceletých středních škol. Vedle učitelů všeobecných i odborných předmětů jsou v datech také pedagogové na konzervatořích. Bokem zůstali metodici, vychovatelé a lektoři žáků se speciálními vzdělávacími potřebami.

<wide><div id="2stupenvek" style="width:100%; height:600px"></div></wide>

<wide><div id="2stupenpohlavi" style="width:100%; height:600px"></div></wide>

<wide><div id="2stupenzamestnani" style="width:100%; height:600px"></div></wide>

Grafy ukazují oproti učitelům na prvním stupni několik odlišností:

* Muži mají o tisícovku, ženy o dva tisíce vyšší průměrný plat.
* Mužů je na druhém stupni necelá třetina.
* Vyšší podíl mužů znamená například plynulejší křivku podílu učitelů podle věku. Na druhém stupni a středních školách je proto relativně víc učitelů kolem třicítky. I tady je ale nejvíc padesátníků.
* Platy a odměny mají podobnou strukturu jako na prvním stupni.
* Křivka počtu učitelů podle délky zaměstnání na jedné škole klesá – na rozdíl od prvního stupně – mnohem pomaleji. Ukazuje, že pokud už učitel vydrží na jedné škole pět let, velmi pravděpodobně ji v nejbližších letech neopustí.

Srovnání Česka a evropských zemí v oblasti primárního a sekundárního školství ukazuje interaktivní mapa organizace IDEA.

<wide><iframe src="https://vitekzkytek.github.io/PlatyUcitelu/" width="100%" height="700px" frameborder="0" scrolling="no"></iframe></wide>